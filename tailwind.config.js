/** @type {import('tailwindcss').Config} */
export default {
	content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
	theme: {
		extend: {
			colors: {
				moss: {
					light: "#5F7357",
					DEFAULT: "#20261B",
					dark: "#131D11",
				},
			},
			animation: {
				more: "up 1s ease-in-out",
				less: "down 1s ease-in-out",
				equal: "same 1s ease-in-out",
			},
			keyframes: {
				up: {
					"0%, 100%": {
						color: "#FFFFFF",
					},
					"50%": {
						color: "#00FF00",
					},
				},
				down: {
					"0%, 100%": {
						color: "#FFFFFF",
					},
					"50%": {
						color: "#FF0000",
					},
				},
				same: {
					"0%, 100%": {
						color: "#FFFFFF",
					},
					"50%": {
						color: "#C4C4C4",
					},
				},
			},
		},
	},
	plugins: [],
};
