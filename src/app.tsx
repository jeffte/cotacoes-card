import { useEffect, useRef, useState } from "preact/hooks";

import { api } from "./services/axios";

import { Card } from "./components/card";

type Json = Record<string, unknown>;

type RawPrices = {
	nome_ativo?: string;
	und_medida?: string;
	bolsa?: string;
	imagem?: string;
	ref_txt?: string;
	referencia?: string;
	simbolo?: string;
	valor_ult: number;
	valor_dif?: string;
	valor_per?: string;
	valor_ant: number;
	atualizacao?: string;
	data_ref?: string;
	data_venc?: string;
};

type Price = {
	reference: string;
	price: number;
	lastPrice: number;
	currency: string;
	variation: number;
	variationPercentage: string;
};

type Prices = {
	country?: string;
	name: string;
	updatedAt: string;
	values: Price[];
};

export function App() {
	const [prices, setPrices] = useState<Prices[]>([]);

	const [refreshTime, setRefreshTime] = useState(3000);

	const priceSequence = useRef<number[]>([]);

	const rootDiv = document.getElementById("app");

	function getRawValues(obj: Json): RawPrices[] {
		return Object.values(obj) as RawPrices[];
	}

	function getParsedValues(obj: RawPrices): Prices {
		const aux = getRawValues(obj);

		return {
			name: aux[0].nome_ativo ?? "",
			updatedAt: aux[0].data_ref ?? "",
			country: aux[0].imagem !== "None" ? aux[0].imagem : undefined,
			values: aux.map(getParsedPrice),
		};
	}

	function getParsedPrice(obj: RawPrices): Price {
		return {
			reference: obj.referencia ?? "",
			price: obj.valor_ult ?? 0,
			lastPrice: obj.valor_ant ?? 0,
			currency: obj.simbolo ?? "",
			variation: Number(obj.valor_dif) || 0,
			variationPercentage: obj.valor_per ?? "",
		};
	}

	if (rootDiv) {
		const refreshTime = rootDiv.dataset.refreshTime || 3000;
		const dataPriceSequence = rootDiv.dataset.priceSequence?.split(",") || [];

		const parsedPriceSequence = dataPriceSequence.map(Number);

		priceSequence.current = parsedPriceSequence;

		setRefreshTime(Number(refreshTime));
	}

	async function fetchData() {
		const { data } = await api.get("/cotacoes");

		const raw = getRawValues(data);

		if (!priceSequence.current.length) {
			return setPrices(raw.map(getParsedValues));
		}

		const filtered = raw.filter((_price, index) =>
			priceSequence.current.includes(index),
		);

		return setPrices(filtered.map(getParsedValues));
	}

	useEffect(() => {
		fetchData();
	}, []);

	useEffect(() => {
		const interval = setInterval(fetchData, Number(refreshTime));

		return () => clearInterval(interval);
	}, [prices]);

	return (
		<div className="bg-moss">
			<main className="container mx-auto text-white py-8">
				<section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-2 px-2">
					{prices.map((price, index) => (
						<Card
							// biome-ignore lint/suspicious/noArrayIndexKey: <explanation>
							key={index}
							country={price.country}
							name={price.name}
							updatedAt={price.updatedAt}
							values={price.values}
						/>
					))}
				</section>
			</main>
		</div>
	);
}
