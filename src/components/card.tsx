import { useEffect, useState } from "preact/hooks";
import { clsx } from "clsx";
import {
	LineChart,
	Line,
	CartesianGrid,
	XAxis,
	ResponsiveContainer,
	Tooltip,
} from "recharts";

type Props = {
	country?: string;
	name: string;
	updatedAt: string;
	values: Price[];
};

type Price = {
	reference: string;
	price: number;
	lastPrice: number;
	currency: string;
	variation: number;
	variationPercentage: string;
};

export function Card({ country, name, updatedAt, values }: Props) {
	const [showGraph, setShowGraph] = useState(false);
	const [showMorePrices, setShowMorePrices] = useState(false);
	const [prices, setPrices] = useState<Price[]>([]);

	function toggleGraph() {
		setShowGraph((prev) => !prev);
	}

	function toggleShowMorePrices() {
		setShowMorePrices((prev) => !prev);
	}

	useEffect(() => {
		if (showMorePrices) {
			setPrices(values);
		} else {
			setPrices(values.slice(0, 3));
		}
	}, [showMorePrices, values]);

	return (
		<div className="bg-moss-dark p-4 rounded">
			<header className="uppercase flex justify-between mb-2">
				<div className="flex gap-2 items-center">
					{country && <img src={country} className="w-7 h-5" alt="" />}
					<h1 className="text-xl">{name}</h1>
				</div>

				<button type="button" onClick={toggleGraph}>
					{showGraph ? "fechar curva" : "abrir curva"}
				</button>
			</header>

			{showGraph && (
				<ResponsiveContainer width="100%" height={200}>
					<LineChart data={values}>
						<CartesianGrid stroke="#ccc" />
						<XAxis dataKey="reference" interval="preserveStartEnd" />
						<Line
							type="monotone"
							name="Preço"
							dataKey="price"
							stroke="#FFC000"
						/>
						<Tooltip />
					</LineChart>
				</ResponsiveContainer>
			)}

			<table className="table-auto border-collapse w-full text-center">
				<thead>
					<tr className="uppercase">
						<th>contrato</th>
						<th>valor</th>
						<th>var.abs</th>
						<th>var.%</th>
					</tr>
				</thead>

				<tbody>
					{prices.map((value, index) => {
						const borderClasses = clsx({
							"border-l-4": true,
							"border-yellow-500": value.variation === 0,
							"border-green-500": value.variation > 0,
							"border-red-500": value.variation < 0,
						});

						const textClasses = clsx({
							"text-yellow-500": value.variation === 0,
							"text-green-500": value.variation > 0,
							"text-red-500": value.variation < 0,
						});

						return (
							<tr
								// biome-ignore lint/suspicious/noArrayIndexKey: <explanation>
								key={index}
								className="uppercase border-t border-slate-50"
							>
								<td className="py-2">
									<p className={borderClasses}>{value.reference}</p>
								</td>
								<td
									className={clsx({
										"animate-more": value.price > value.lastPrice,
										"animate-less": value.price < value.lastPrice,
										"animate-equal": value.price === value.lastPrice,
									})}
								>
									{value.currency} {value.price}
								</td>
								<td className={textClasses}>{value.variation || "-"}</td>
								<td className={textClasses}>{value.variationPercentage}</td>
							</tr>
						);
					})}
				</tbody>
			</table>

			<footer className="flex justify-between mt-2">
				<button type="button" onClick={toggleShowMorePrices}>
					{showMorePrices ? "- contratos" : "+ contratos"}
				</button>
				<span className="italic">Atualizado em: {updatedAt}</span>
			</footer>
		</div>
	);
}
